#### This is a PKGBUILD study git for a personal repository for ArchLinux, but you are welcome to use it

<div align="center">

[<img src="https://i.imgur.com/UjnRFbR.png" width="250">](https://archlinux.org/)

</div>


<div align="center" font-size="12px">

Copyright © 2002-2024 Judd Vinet, Aaron Griffin and Levente Polyák.
The [Arch Linux](https://archlinux.org/) name and logo are recognized trademarks. Some rights reserved.
[Linux®](https://www.kernel.org/) is a registered trademark of Linus Torvalds.

</div>
